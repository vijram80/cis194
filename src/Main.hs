module Main where

import qualified Week1.HW01 as W1
import qualified Week1.HW01Tests as W1T
import qualified Week2.HW02 as W2
import qualified Week2.HW02Tests as W2T
import qualified Week3.HW03 as W3
import qualified Week4.HW04 as W4
import qualified Week4.HW04Tests as W4T
import qualified Week5.HW05 as W5 hiding (main)
import qualified Week5.HW05Tests as W5T
import qualified Week6.HW06 as W6
import qualified Week7.HW07 as W7 hiding (main)
import Testing

main :: IO ()
main = do
  putStrLn "Run the tests"
  print $ runTests W1T.allTests
  print $ runTests W2T.allTests
  print $ runTests W4T.allTests
  key <- W5.getSecret "src/Week5/data/dog.jpg" "src/Week5/data/dog-original.jpg"
  W5.decryptWithKey key "src/Week5/data/victims.json.enc"

-- give it a file name with sample credit card data like "src/Week1/cc.txt"
-- file was created with numbers generated at http://www.getcreditcardnumbers.com/
testCCFile :: String -> IO [Bool]
testCCFile fn = map (W1.luhn . W1.strToInteger) . words <$> readFile fn

-- so we can call it unambiguously from REPL (HW05 also has a main)
main' :: IO ()
main' = main

w5Main :: IO String
w5Main = W5.doEverything
          "src/Week5/data/dog-original.jpg"
          "src/Week5/data/dog.jpg"
          "src/Week5/data/transactions.json"
          "src/Week5/data/victims.json"
          "src/Week5/data/new-ids.json"
          "src/Week5/data/new-transactions.json"
