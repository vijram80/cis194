{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE OverloadedStrings, RecordWildCards #-}
module Week5.HW05 where

import Week5.Parser

import Data.ByteString.Lazy (ByteString)
import Data.Map.Strict (Map)
import System.Environment (getArgs)
import Data.Bits as Bits
import Data.Maybe
import Data.List (sortBy)

import qualified Data.ByteString.Lazy as BS
import qualified Data.Map.Strict as Map


getSecret :: FilePath -> FilePath -> IO ByteString
getSecret file1 file2 = do
  contents1 <- BS.readFile file1
  contents2 <- BS.readFile file2
  return $ BS.filter (/= 0) $ BS.pack $ BS.zipWith xor contents1 contents2


decryptWithKey :: ByteString -> FilePath -> IO ()
decryptWithKey key file = do
  contents <- BS.readFile $ file  ++ ".enc"
  BS.writeFile file $ BS.pack $ BS.zipWith xor (BS.concat $ repeat key) contents


testdecrypt :: FilePath -> FilePath -> FilePath -> IO ()
testdecrypt file1 file2 file = do
  key <- getSecret file1 file2
  decryptWithKey key file


parseFile :: FromJSON a => FilePath -> IO (Maybe a)
parseFile file = do
  contents <- BS.readFile file
  return $ decode contents

-- testParseFile file = parseFile file :: IO (Maybe [Transaction])

readTransactions :: [Transaction] -> TId -> Maybe Transaction
readTransactions [] _ = Nothing
readTransactions (Transaction a b c tid: transactions) vId = if tid == vId then Just (Transaction a b c tid) else readTransactions transactions vId


getBadTs :: FilePath -> FilePath -> IO (Maybe [Transaction])
getBadTs file1 file2 = do
  tids <- parseFile file1 :: IO (Maybe [TId])
  trans <- parseFile file2 :: IO (Maybe [Transaction])
  return $ mapM (readTransactions (fromMaybe [] trans)) (fromMaybe [] tids)

getFlow :: [Transaction] -> Map String Integer
getFlow [] = Map.empty
getFlow (Transaction fr to am _ : transactions) = Map.insertWith (+) fr (-am) $ Map.insertWith (+) to am $ getFlow transactions

getCriminal :: Map String Integer -> String
getCriminal flow = let amt = head.sortBy (flip compare) $ Map.elems flow in
                   foldl (\acc kv -> if amt == snd kv then fst kv else acc ) "" $ Map.assocs flow


undoTs :: Map String Integer -> [TId] -> [Transaction]
undoTs flow tids = undo (payers flow) (payees flow) tids where
                    undo :: [(String, Integer)] -> [(String, Integer)] -> [TId] -> [Transaction]
                    undo _ [] _ = []
                    undo [] _ _ = []
                    undo _ _ [] = []
                    undo ((fr, fAmt): frecord) ((to, tAmt) : trecord) (tid:trIds) = if tAmt <= fAmt then Transaction fr to (negate tAmt) tid
                                                                                  : undo ((fr,fAmt - tAmt) : frecord) trecord trIds
                                                                                  else undo frecord ((to,tAmt):trecord) (tid:trIds)


sortList :: [(String, Integer)] -> [(String, Integer)]
sortList = sortBy (\(_, v1) (_,v2) -> v2 `compare` v1)

payees :: Map String Integer -> [(String, Integer)]
payees flow = sortList $ Map.toList $ foldl (\acc kv -> if snd kv < 0 then uncurry Map.insert kv acc else acc ) Map.empty $ Map.assocs flow

payers :: Map String Integer -> [(String, Integer)]
payers flow = sortList $ Map.toList $ foldl (\acc kv -> if snd kv > 0 then uncurry Map.insert kv acc else acc ) Map.empty $ Map.assocs flow

writeJSON :: ToJSON a => FilePath -> a -> IO ()
writeJSON file bdata = BS.writeFile file $ encode bdata


doEverything :: FilePath -> FilePath -> FilePath -> FilePath -> FilePath
             -> FilePath -> IO String
doEverything dog1 dog2 trans vict fids out = do
  key <- getSecret dog1 dog2
  decryptWithKey key vict
  mts <- getBadTs vict trans
  case mts of
    Nothing -> error "No Transactions"
    Just ts -> do
      mids <- parseFile fids
      case mids of
        Nothing  -> error "No ids"
        Just ids -> do
          let flow = getFlow ts
          writeJSON out (undoTs flow ids)
          return (getCriminal flow)


w5Main :: IO String
w5Main = doEverything
          "Week5/data/dog-original.jpg"
          "Week5/data/dog.jpg"
          "Week5/data/transactions.json"
          "Week5/data/victims.json"
          "Week5/data/new-ids.json"
          "Week5/data/new-transactions.json"


main :: IO ()
main = do
  args <- getArgs
  crim <-
    case args of
      dog1:dog2:trans:vict:ids:out:_ ->
          doEverything dog1 dog2 trans vict ids out
      _ -> doEverything "dog-original.jpg"
                        "dog.jpg"
                        "transactions.json"
                        "victims.json"
                        "new-ids.json"
                        "new-transactions.json"
  putStrLn crim
