{-# OPTIONS_GHC -Wall #-}
module Week4.HW04 where

import Data.List

newtype Poly a = P [a]

x :: Num a => Poly a
x = P [0, 1]

instance (Num a, Eq a, Ord a) => Eq (Poly a) where
  (P p1) == (P p2) = dropZeros p1 == dropZeros p2
    where dropZeros z = filter (>= 1) $ reverse z


instance (Num a, Eq a, Show a, Ord a) => Show (Poly a) where
    show (P [])= "0"
    show (P [0]) = "0"
    show p = intercalate " + " $ filter (not . null) $ reverse $ addexp p


showPoly :: (Num a, Eq a, Show a, Ord a) => Poly a -> [String]
showPoly (P plist) = map showP plist
      where showP 1 = "1"
            showP 0 = ""
            showP (-1) = "-x"
            showP c | c < 0 = show c ++ "x"
                    | otherwise = show c ++ "x^"

addexp1 :: [String] -> [String]
addexp1 [] = []
addexp1 (y:ys) = if '^' `elem` y then show (y ++ show (length ys)) : addexp1 ys
                else y : addexp1 ys

addexp :: (Num a, Ord a, Show a) => Poly a -> [String]
addexp p = concat [[if b > 1 then if a /= "" && '^' `elem` a then a ++ show b else "" else a ] | (a,b) <- zip (showPoly p) ([0..] :: [Integer])]

plus :: Num a => Poly a -> Poly a -> Poly a
plus (P ys) (P zs) = P (add ys zs)
                     where
                       add [] [] = []
                       add as [] = as
                       add [] bs = bs
                       add (a:as) (b:bs) = a + b : add as bs

times :: Num a => Poly a -> Poly a -> Poly a
times (P ys) (P zs) = sum (time ys zs)
                      where
                        time :: Num a => [a] -> [a] -> [Poly a]
                        time _ [] = []
                        time [] _ = []
                        time (c:cs) bs = P (map (*c) bs) : time cs (0 : bs)


neg :: Num a => Poly a -> Poly a
neg (P xs) = P (map (\c -> -c) xs)

fromInt :: Num a => Integer -> Poly a
fromInt i = P [fromInteger i]

instance Num a => Num (Poly a) where
    (+) = plus
    (*) = times
    negate (P xs) = P $ map negate xs
    fromInteger n = P [fromIntegral n]
    -- No meaningful definitions exist
    abs    = undefined
    signum = undefined

applyP :: Num a => Poly a -> a -> a
applyP (P xs) num = sum (map (* num) xs)


class Num a => Differentiable a where
    deriv  :: a -> a
    nderiv :: Int -> a -> a
    nderiv 1 f = deriv f
    nderiv n f = nderiv (n-1) (deriv f)


deriv1 :: (Num a, Enum a) => Poly a -> Poly a
deriv1 (P xs) = P (zipWith (*) (tail xs) [1..])

instance (Num a, Enum a) => Differentiable (Poly a) where
    deriv  = deriv1
