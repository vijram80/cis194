{-# OPTIONS_GHC -Wall #-}
module Week1.HW01 where

import Data.Char (ord)

-- Exercise 1 -----------------------------------------

charToInteger :: Char -> Integer
charToInteger = subtract 48 . toInteger . ord

strToInteger :: String -> Integer
strToInteger = foldl (\t v -> (10*t) + charToInteger v) 0

-- Get the last digit from a number
toDigits :: Integer -> [Integer]
toDigits x = map charToInteger $ show x

lastDigit :: Integer -> Integer
lastDigit = last . toDigits

-- Drop the last digit from a number
dropLastDigit :: Integer -> Integer
dropLastDigit = (`div` 10)

-- Exercise 2 -----------------------------------------

toRevDigits :: Integer -> [Integer]
toRevDigits x
  | x <= 0 = []
  | otherwise = reverse $ toDigits x

-- Exercise 3 -----------------------------------------

-- Double every second number in a list starting on the left.
doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther = zipWith (*) (cycle [1,2])

-- Exercise 4 -----------------------------------------

-- Calculate the sum of all the digits in every Integer.
sumDigits :: [Integer] -> Integer
sumDigits xs = sum $ concatMap toDigits xs

-- Exercise 5 -----------------------------------------

-- Validate a credit card number using the above functions.
luhn :: Integer -> Bool
luhn x = mod (summ x) 10 == 0 where
  summ = sumDigits . doubleEveryOther . toRevDigits

-- Exercise 6 -----------------------------------------

-- Towers of Hanoi for three pegs
type Peg = String
type Move = (Peg, Peg)
type PegState = (Peg, [Int])

hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
-- hanoi n p1 p2 p3 =
--   let pegStates = [(p1,[1..n]), (p2,[]), (p3, [])] in
--     foldr (\v t -> (fst v, fst v) : t) [] pegStates

hanoi 0 _ _ _ = []
hanoi 1 a b _ = [(a, b)]
hanoi n a b c = hanoi (n - 1) a c b ++
                hanoi 1 a b c ++
                hanoi (n - 1) c b a
