{-# LANGUAGE MonadComprehensions, RecordWildCards #-}
{-# OPTIONS_GHC -Wall #-}
module Week7.HW07 where

import Prelude hiding (mapM)
import Week7.Cards

import Control.Monad hiding (mapM, liftM)
import Control.Monad.Random
import Data.Functor
import Data.Monoid
import Data.Vector (Vector, cons, (!), (!?), (//))
import System.Random
import Data.Maybe

import qualified Data.Vector as V


liftM :: Monad m => (a -> b) -> m a -> m b
liftM f x = pure f <*> x

swapV :: Num a =>  Int -> Int -> Vector a -> Maybe (Vector a)
swapV x y c = do
  a <- c !? x
  b <- c !? y
  return $ c // [(y, a), (x, b)]

mapM :: Monad m => (a -> m b) -> [a] -> m [b]
mapM fn = sequence . liftM fn

getElts :: [Int] -> Vector a -> Maybe [a]
getElts xs vec = mapM (vec !?) xs

type Rnd a = Rand StdGen a

randomElt :: Vector a -> Rnd (Maybe a)
randomElt c = (c !?) <$> getRandomR (0, V.length c)

randomVec :: Random a => Int -> Rnd (Vector a)
randomVec n = V.replicateM n getRandom

randomVecR :: Random a => Int -> (a, a) -> Rnd (Vector a)
randomVecR n (lo,hi) = V.replicateM n (getRandomR (lo, hi))


shuffle :: Vector a -> Rnd (Vector a)
shuffle c = shuffle'' c V.empty


shuffle'' :: Vector a -> Vector a -> Rnd (Vector a)
shuffle'' c b | V.null c = return b
              | otherwise = do
                            k <- getRandomR (0, V.length c - 1)
                            let (newc, newb) = V.splitAt k c in shuffle'' (newc V.++ V.drop 1 newb) (V.cons (V.head newb) b)


partitionAt :: Ord a => Vector a -> Int -> (Vector a, a, Vector a)
partitionAt c idx = partition c (c ! idx) idx

partition :: Ord a => Vector a -> a -> Int -> (Vector a, a, Vector a)
partition c n idx = (less, n, great) where
  less = V.filter (< n) c
  great = let (x,y) = V.splitAt idx c in V.filter (>= n) (x V.++ V.drop 1 y)



quicksort :: Ord a => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = quicksort [ y | y <- xs, y < x ]
                   <> (x : quicksort [ y | y <- xs, y >= x ])

qsort :: Ord a => Vector a -> Vector a
qsort c | V.null c = c
        | otherwise = V.fromList (quicksort $ V.toList c)


qsortR :: Ord a => Vector a -> Rnd (Vector a)
qsortR c | V.null c = return c
         | otherwise = do
           idx <- getRandomR(0, V.length c - 1)
           let (left, elt, right) = partitionAt c idx
           lsort <- qsortR left
           rsort <- qsortR right
           return $ lsort V.++ V.cons elt rsort


select :: Ord a => Int -> Vector a -> Rnd (Maybe a)
select n c | V.null c = return Nothing
           | otherwise = do
             idx <- getRandomR(0, V.length c - 1)
             let (left, elt, right) = partitionAt c idx
             if V.length left < n then select n left else if V.length left > n then select n right else return $ Just elt


allCards :: Deck
allCards =  do
  label <- labels
  suit <- suits
  return $ Card label suit

newDeck :: Rnd Deck
newDeck =  shuffle allCards

nextCard :: Deck -> Maybe (Card, Deck)
nextCard cards | V.null cards = Nothing
               | otherwise = Just (V.head cards, V.tail cards)

getCards :: Int -> Deck -> Maybe ([Card], Deck)
getCards n cards | V.null cards = Nothing
                 | V.length cards < n = Nothing
                 | otherwise = drawCards [] cards where
                   drawCards acc cards' | length acc > n = return (acc, cards')
                                        | otherwise = do
                                                      (card, deck) <- nextCard cards'
                                                      drawCards (card : acc) deck

data State = State { money :: Int, deck :: Deck }

repl :: State -> IO ()
repl s@State{..} | money <= 0  = putStrLn "You ran out of money!"
                 | V.null deck = deckEmpty
                 | otherwise   = do
  putStrLn $ "You have \ESC[32m$" ++ show money ++ "\ESC[0m"
  putStrLn "Would you like to play (y/n)?"
  cont <- getLine
  if cont == "n"
  then putStrLn $ "You left the casino with \ESC[32m$"
           ++ show money ++ "\ESC[0m"
  else play
    where deckEmpty = putStrLn $ "The deck is empty. You got \ESC[32m$"
                      ++ show money ++ "\ESC[0m"
          play = do
            putStrLn "How much do you want to bet?"
            amt <- read <$> getLine
            if amt < 1 || amt > money
            then play
            else do
              case getCards 2 deck of
                Just ([c1, c2], d) -> do
                  putStrLn $ "You got:\n" ++ show c1
                  putStrLn $ "I got:\n" ++ show c2
                  case () of
                    _ | c1 >  c2  -> repl $ State (money + amt) d
                      | c1 <  c2  -> repl $ State (money - amt) d
                      | otherwise -> war s{deck = d} amt
                _ -> deckEmpty
          war (State m d) amt = do
            putStrLn "War!"
            case getCards 6 d of
              Just ([c11, c21, c12, c22, c13, c23], d') -> do
                putStrLn $ "You got\n" ++ ([c11, c12, c13] >>= show)
                putStrLn $ "I got\n" ++ ([c21, c22, c23] >>= show)
                case () of
                  _ | c13 > c23 -> repl $ State (m + amt) d'
                    | c13 < c23 -> repl $ State (m - amt) d'
                    | otherwise -> war (State m d') amt
              _ -> deckEmpty

w7main :: IO ()
w7main = evalRandIO newDeck >>= repl . State 100
