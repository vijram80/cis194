{-# OPTIONS_GHC -Wall #-}
module Week6.HW06 where

import Data.List
import Data.Functor



fib :: Integer -> Integer
fib 0 = 1
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

fibs1 :: [Integer]
fibs1 = map fib [0..]


fibs2 :: [Integer]
fibs2 = 1 : 1 : zipWith (+) (tail fibs2) fibs2

data Stream a = Cons a (Stream a)

-- Show instance prints the first 20 elements followed by ellipsis
instance Show a => Show (Stream a) where
    show s = "[" ++ intercalate ", " (map show $ take 10 $ streamToList s)
             ++ ",..."


streamToList :: Stream a -> [a]
streamToList (Cons s st) = s : streamToList st

instance Functor Stream where
    fmap f (Cons s st ) = Cons (f s) (fmap f st)


sRepeat :: a -> Stream a
sRepeat x = Cons x (sRepeat x)

sIterate :: (a -> a) -> a -> Stream a
sIterate f x = Cons x $ (sIterate f . f) x

sInterleave :: Stream a -> Stream a -> Stream a
sInterleave (Cons s1 st1) st2 = Cons s1 $ sInterleave st2 st1

sTake :: Int -> Stream a -> [a]
sTake 0 _ = []
sTake n (Cons s st) = s : sTake (n-1) st

nats :: Stream Integer
nats = sIterate (1 +) 0

rule :: Integer -> Stream Integer
rule n = sInterleave (sRepeat n) (rule $ n+1)

ruler :: Stream Integer
ruler = rule 0

rand :: Int -> Stream Int
rand n = sIterate calc n
         where calc n1 = (1103515245 * n1 + 12345) `mod` 214783648

{- Total Memory in use: ??? MB -}
minMaxSlow :: [Int] -> Maybe (Int, Int)
minMaxSlow [] = Nothing   -- no min or max if there are no elements
minMaxSlow xs = Just (minimum xs, maximum xs)

-- fibs2 = 1 : 1 : zipWith (+) (tail fibs2) fibs2
{- Total Memory in use: ??? MB -}
minMax :: [Int] -> Maybe (Int, Int)
minMax [] = Nothing
minMax (x:y:xs) = helper xs (x,y)
                where helper [] (a, b) = Just (a, b)
                      helper (z:zs) (a, b) | z < a = helper zs (z, b)
                                           | z > b = helper zs (a, z)
                                           | otherwise = helper zs (a, b)


w6Main :: IO ()
w6Main = print $ minMax $ sTake 1000000 $ rand 7666532

-- Exercise 10 ----------------------------------------

fastFib :: Int -> Integer
fastFib = undefined
